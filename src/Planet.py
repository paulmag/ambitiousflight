import numpy as np


class Planet:

    def __init__(self, pos, mass, radius):
        self.pos = np.array(pos)
        self.mass = float(mass)
        self.radius = int(radius)
        self.world = None
        self.game = None
        G = 1000.  # TODO Set this constant properly.
        self.scale_height = 10000. / (G * self.mass / radius**2)
        self.density_surface = 2 * np.random.uniform()

    def get_density(self, pos):
        r_abs = np.linalg.norm(pos - self.pos) - self.radius
        return self.density_surface * np.exp(- (r_abs) / self.scale_height)
