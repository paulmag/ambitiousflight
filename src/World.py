from Plane import Plane
from Planet import Planet
from Star import Star

class World:

    METER_SIZE = 10  # Number of pixels per square side.
    VOID_COLOR = (150, 30, 255)


    def __init__(self, background=VOID_COLOR):
        self.background = background
        self.planes = []
        self.planets = []
        self.stars = []
        self.game = None



    def add(self, thing):
        if isinstance(thing, Planet):
            self.planets.append(thing)
            thing.world = self
            thing.game = self.game
        elif isinstance(thing, Star):
            self.stars.append(thing)
            thing.world = self
            thing.game = self.game
        elif isinstance(thing, Plane):
            self.planes.append(thing)
            thing.world = self
            thing.game = self.game
