import sys
import numpy as np
import pygame
from pygame.locals import *

from World import World
from Plane import Plane
from Planet import Planet
from Star import Star


class Game:


    def __init__(self):
        self.FPS = 30
        self.dt = 1. / self.FPS
        pygame.init()
        self.clock = pygame.time.Clock()
        self.width = 800
        self.height = 800
        self.screen = pygame.display.set_mode((
            self.width, self.height
        ))
        pygame.display.set_caption('Testing')
        self.world = World()
        self.world.game = self
        self.player = Plane(pos=[200,20], velocity=[33,0])
        self.world.add(self.player)
        self.world.add(Planet([200,200], 500, 30))
        self.world.add(Planet([600,200], 500, 40))
        self.world.add(Planet([400,-1000], 15000, 500))
        self.world.add(Planet([400,800], 1000, 50))
        for i in xrange(200):
            depth = np.random.uniform() * 20 + 2
            pos = (np.random.uniform(size=2) * 1600 - self.width/2) * depth
            self.world.add(Star(pos, depth=depth, radius=10))


    def start(self):
        self.world.planes[0].set_planets()
        self._draw()
        while 1:
            self._update()
            self._draw()
            self.dt = self.clock.tick(self.FPS) * 0.001
            print self.clock.get_fps()


    def _update(self):
        ROT_FACTOR = 2.  #TODO Where to define this and what to call it?
        THRUST_FACTOR = 100.  #TODO Set this constant properly.

        for event in pygame.event.get(): # event handling loop
            if event.type == QUIT:
                pygame.quit()
                sys.exit()
            elif event.type == KEYDOWN:
                if event.key == K_LEFT or event.key == K_a:
                    self.player.pitch_acceleration += ROT_FACTOR
                elif event.key == K_RIGHT or event.key == K_d:
                    self.player.pitch_acceleration -= ROT_FACTOR
                elif event.key == K_UP or event.key == K_w:
                    self.player.thrust += THRUST_FACTOR
                elif event.key == K_DOWN or event.key == K_s:
                    pass
                elif event.key == K_l:
                    # self.player.sprinting = True
                    pass
            elif event.type == KEYUP:
                if event.key == K_LEFT or event.key == K_a:
                    self.player.pitch_acceleration -= ROT_FACTOR
                elif event.key == K_RIGHT or event.key == K_d:
                    self.player.pitch_acceleration += ROT_FACTOR
                elif event.key == K_UP or event.key == K_w:
                    self.player.thrust -= THRUST_FACTOR
                elif event.key == K_DOWN or event.key == K_s:
                    pass
                elif event.key == K_l:
                    # self.player.sprinting = False
                    pass

        # self.player.pitch_speed += self.player.pitch_acceleration * self.dt
        self.player.set_planets()
        self.player.set_acceleration()
        self.player.update()


    def _draw(self):
        player_sprite_pos = self.player.pos - self.player.sprite_center
        offset = (
            np.array([self.width, self.height]) / 2 -
            self.player.pos.astype(int)
        )  # For centering camera on player position.
        for s in self.world.stars:
            pygame.draw.circle(
                self.screen,
                (255,255,255),
                ((s.pos - self.player.pos) / s.depth).astype(int) +
                    np.array([self.width, self.height]) / 2,
                s.radius,
            )
        for p in self.world.planets:
            pygame.draw.circle(
                self.screen, (127,127,127), p.pos+offset, p.radius
            )
            pygame.draw.circle(
                self.screen,
                p.density_surface*np.array([0,50,100,0]),
                p.pos+offset,
                p.radius + int(p.scale_height),
                int(p.scale_height),
            )
        self.screen.blit(
            self.player.get_sprite(),
            player_sprite_pos+offset,
        )
        pygame.display.update()
        for s in self.world.stars:
            pygame.draw.circle(
                self.screen,
                (0,0,0),
                ((s.pos - self.player.pos) / s.depth).astype(int) +
                    np.array([self.width, self.height]) / 2,
                s.radius,
            )
        for p in self.world.planets:
            pygame.draw.circle(
                self.screen,
                (0,0,0),
                p.pos+offset,
                p.radius + int(p.scale_height + 1)
            )
        makeCell = pygame.Rect(player_sprite_pos+offset, self.player.sprite_shape)
        pygame.draw.rect(
            self.screen,
            (0,0,0),
            makeCell,
        )


if __name__ == "__main__":
    game = Game()
    game.start()
