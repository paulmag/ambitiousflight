import numpy as np
import pygame


class Plane:

    def __init__(self, pos, velocity=[0,0], pitch=0, mass=1.):
        self.mass = float(mass)
        self.pos = np.array(pos)
        self.velocity = np.array(velocity)
        self.acceleration = np.array([0,0])
        self.thrust = 0.
        self.pitch = float(pitch)
        self.pitch_velocity = 0.
        self.pitch_acceleration = 0.
        self.planets = []
        self.world = None
        self.game = None
        self.sprite = pygame.image.load('../images/rocket.png').convert_alpha()
        self.sprite_thrust = pygame.image.load('../images/rocket_thrust.png').convert_alpha()
        self.sprite = pygame.transform.rotate(self.sprite, -90)
        self.sprite_thrust = pygame.transform.rotate(self.sprite_thrust, -90)
        self.sprite_center = np.array(self.sprite.get_rect().center)
        self.sprite_shape = np.array(self.sprite.get_rect().bottomright)
        self.radius = self.sprite_center[0]  # Derive radius from size of sprite.

    def get_sprite(self):
        # Rotate the sprite while keeping its center and size.
        if self.thrust > 0:
            rot_image = pygame.transform.rotate(
                self.sprite_thrust, self.pitch*180/np.pi
            )
        else:
            rot_image = pygame.transform.rotate(
                self.sprite, self.pitch*180/np.pi
            )
        rot_rect = self.sprite.get_rect().copy()
        rot_rect.center = rot_image.get_rect().center
        rot_image = rot_image.subsurface(rot_rect).copy()
        return rot_image

    def set_planets(self):
        # Make list of the nearest planets.
        distances = []
        for planet in self.world.planets:
            distances.append(np.linalg.norm(self.pos - planet.pos))
        self.planets = []
        for i in np.argsort(distances)[:3]:  # Max 3 planets necessary.
            self.planets.append(self.world.planets[i])

    def set_acceleration(self):
        self.acceleration = (
            self._get_gravity() +
            self._get_thrust() +
            self._get_drag() +
            self._get_lift() +
            self._get_bouyancy() +
            self._get_collision()
        ) / self.mass

    def update(self):
        self.velocity = self.velocity + self.acceleration * self.game.dt
        self.pos = self.pos + self.velocity * self.game.dt
        self.pitch_velocity += self.pitch_acceleration * self.game.dt
        self.pitch += self.pitch_velocity * self.game.dt

    def _get_gravity(self):
        s = np.array([0, 0])
        G = 1000.  # TODO Set this constant properly.
        for planet in self.planets:
            r = self.pos - planet.pos
            s = s - G * self.mass * planet.mass * r / np.linalg.norm(r)**3
        return s

    def _get_thrust(self):
        rotation_matrix = np.matrix([
            [  np.cos(self.pitch), np.sin(self.pitch)],
            [- np.sin(self.pitch), np.cos(self.pitch)],
        ])
        s = np.asarray(
            rotation_matrix * np.array([[self.thrust],[0]])
        ).transpose()[0]
        return s

    def _get_drag(self):
        s = np.array([0, 0])
        DRAG_COEFFICIENT = .002
        area = 10.
        for planet in self.planets[:2]:
            s = s - (
                0.5 *
                planet.get_density(self.pos) *
                self.velocity.dot(self.velocity) *
                self.velocity / np.linalg.norm(self.velocity) *
                DRAG_COEFFICIENT *
                area
            )
        return s

    def _get_lift(self):
        s = np.array([0, 0])
        for planet in self.planets[:1]:
            s = s + np.array([0, 0])
        return s

    def _get_bouyancy(self):
        s = np.array([0, 0])
        for planet in self.planets[:1]:
            s = s + np.array([0, 0])
        return s

    def _get_collision(self):
        s = np.array([0, 0])
        for planet in self.planets[:1]:
            r = self.pos - planet.pos
            r_abs = np.linalg.norm(r) - self.radius
            if r_abs < planet.radius:
                theta = np.arctan2(r[1], r[0])
                x = self.radius * np.cos(theta)
                y = self.radius * np.sin(theta)
                r = r - [x, y]  # Only use time one this when collision.
                self.velocity *= 1. - 6. * self.game.dt
                self.pitch_velocity *= 1. - 6. * self.game.dt
                s = s + 1000. * (planet.radius - r_abs) * r / r_abs
                    #TODO Where to get "bounce" constant from?
        return s
