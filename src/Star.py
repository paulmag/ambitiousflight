import numpy as np


class Star:

    def __init__(self, pos, depth, radius):
        self.pos = np.array(pos)
        self.depth = float(depth)
        self.radius = int(np.ceil(radius / depth))
        self.world = None
        self.game = None

